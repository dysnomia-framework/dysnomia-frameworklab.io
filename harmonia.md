---
layout: page
title: harmonia
permalink: /harmonia
---

[harmonia](https://gitlab.com/dysnomia-framework/harmonia) is a port of the Lapis ORM to dysnomia.

Web documentation is WIP, see the [README](https://gitlab.com/dysnomia-framework/harmonia/blob/master/README.md) in the meantime.
