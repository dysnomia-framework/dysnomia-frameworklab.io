---
layout: page
title: dysnomia
permalink: /dysnomia
---

[dysnomia](https://gitlab.com/dysnomia-framework/dysnomia) is the main framework module.

Web documentation is WIP, see the [README](https://gitlab.com/dysnomia-framework/dysnomia/blob/master/README.md) in the meantime.
