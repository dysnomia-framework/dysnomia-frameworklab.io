---
layout: page
title: About
permalink: /about/
---

This is the documentation for the [Dysnomia framework](https://gitlab.com/dysnomia-framework), a modular openresty and moonscript based web framework.

It is currently composed of the following modules:

* `dysnomia`, the main module and framework, containing logic to actually run your site, and submodules for:
  * `config`, the configuration system
  * `renderers`, most importantly `html5_dsl`, a domain specific language built on top of moonscript rendering to HTML5
  * `routers`, currently `regex_router`, which routes based on regular expressions applied to the URL
  * `utils`, both internally and for your usage, of which `http` is probably the most useful in your app, which is a `luasocket.http.simple` drop in replacement using nginx' internal http client
* `harmonia`, a port of the Lapis ORM, to talk to your database (builtin support for postgresql and mysql, but technically your model could use whatever backend your want, if you implement it)
* `eris`, a shell script to set up dysnomia based projects and run commands in the right environment set up for them.
