---
layout: page
title: eris
permalink: /eris
---

[eris](https://gitlab.com/dysnomia-framework/eris) is the command line tool for managing dysnomia projects.

Web documentation is WIP, see the [README](https://gitlab.com/dysnomia-framework/eris/blob/master/README.md) in the meantime.
